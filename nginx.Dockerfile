FROM nginx:latest

COPY nginx-default.conf.template /etc/nginx/conf.d/default.conf.template
COPY nginx-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
