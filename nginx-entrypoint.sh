#!/bin/bash

if [[ -z "$API_HOST" ]]; then
  export API_HOST=0.0.0.0
fi
if [[ -z "$API_PORT" ]]; then
  export API_PORT=5000
fi

envsubst '${API_HOST} ${API_PORT}' \
  < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
