#!/bin/bash

function checkEnv() {
  if [[ -z "$FLASK_ENV" ]]; then
    echo "FLASK_ENV is not set, using development"
    export FLASK_ENV=development
  fi
  if [[ -z "$FLASK_APP" ]]; then
    echo "FLASK_APP is not set, using app"
    export FLASK_APP=app
  fi
  if [[ -z "$PORT" ]]; then
    echo "PORT is not set, using 5000"
    export PORT=5000
  fi
  if [[ -z "$THREADS" ]]; then
    echo "THREADS is not set, using 4"
    export THREADS=4
  fi
  if [[ -z "$WORKERS" ]]; then
    echo "WORKERS is not set, using 4"
    export WORKERS=4
  fi
}

if [ "$1" = 'start' ]; then
  # Set default env vars
  checkEnv

  # Activate python env and serve app
  . ./env/bin/activate
  gunicorn -b 0.0.0.0:${PORT} \
    --worker-tmp-dir /dev/shm \
    --threads=${THREADS} \
    --workers=${WORKERS} \
    --worker-class=gthread \
    --log-file=- \
    -t 120 app:app --preload
  exit
fi

exec "$@"
