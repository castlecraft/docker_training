# Intro

- flask-demo-docker:latest is flask app which runs gunicorn worker with flask app.
- nginx-demo-docker:latest is nginx reverse proxy that serves backend at `${API_HOST}:${API_PORT}`

# Setup local venv

```shell
# Setup python venv
python -m venv env

# Activate env
. ./env/bin/activate

# Upgrade pip
pip install --upgrade pip

# Install dependencies (flask and gunicorn)
pip install -r requirements.txt
```

# Run flask app

```shell
# Activate env
. ./env/bin/activate

# Run flask app
MESSAGE=success flask run
```

# Use gunicorn to run flask app

```shell
# Activate env
. ./env/bin/activate

# Run gunicorn worker
MESSAGE=success gunicorn -b 0.0.0.0:8000 \
  --worker-tmp-dir /dev/shm \
  --threads=4 \
  --workers=8 \
  --worker-class=gthread \
  --log-file=- \
  -t 120 app:app --preload
```

# Build flask image

```shell
docker build -t flask-demo-docker:latest -f Dockerfile .
```

# Build Nginx Image

```shell
docker build -t nginx-demo-docker:latest -f nginx.Dockerfile .
```

# docker-compose

```shell
docker-compose up # -d # use for daemon mode
```
