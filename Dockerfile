FROM python:slim

COPY . /app

RUN useradd -ms /bin/bash frappe
WORKDIR /app
RUN python -m venv env && \
  ./env/bin/pip install --upgrade pip && \
  ./env/bin/pip install -r requirements.txt && \
  chown -R frappe:frappe /app

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

EXPOSE 5000

USER frappe

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
